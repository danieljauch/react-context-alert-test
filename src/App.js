import React, { Component } from "react"
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import "./App.css"

let initalAlerts = []
const AlertContext = React.createContext()

class AlertProvider extends Component {
  state = {
    alerts: initalAlerts
  }

  createAlert = message => {
    let alerts = [...this.state.alerts]

    alerts.push({
      message,
      dismissed: false,
      index: alerts.length
    })

    this.setState({ alerts })
  }

  dismissAlert = index => {
    let alerts = [...this.state.alerts]

    alerts[index].dismissed = true

    this.setState({ alerts })
  }

  render() {
    return (
      <AlertContext.Provider
        value={{
          state: this.state,
          createAlert: this.createAlert,
          dismissAlert: this.dismissAlert
        }}>
        {this.props.children}
      </AlertContext.Provider>
    )
  }
}

class App extends Component {
  render() {
    return (
      <AlertProvider>
        <Router>
          <AlertContext.Consumer>
            {context => (
              <div className="app__root">
                [App]
                <Route path="/" exact render={() => <Index context={context} />} />
                <Route path="/test" render={() => <Test context={context} />} />
              </div>
            )}
          </AlertContext.Consumer>
        </Router>
      </AlertProvider>
    )
  }
}

class Page extends Component {
  render() {
    return (
      <main className="page__wrapper">
        [Page]
        {this.props.children}
        <Header />
      </main>
    )
  }
}

class Index extends Component {
  componentDidMount = () => {
    setTimeout(() => {
      this.props.context.createAlert("Hi!")
    }, 1000)
  }

  render() {
    return <Page>[Index]</Page>
  }
}

class Test extends Component {
  render() {
    return <Page>[Test]</Page>
  }
}

class Header extends Component {
  render() {
    return (
      <header className="app__header">
        [Header]
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/test">Test</Link>
          </li>
        </ul>
        <Alerts />
      </header>
    )
  }
}

class Alerts extends Component {
  render() {
    return (
      <AlertContext.Consumer>
        {context => (
          <section className="alert__wrapper">
            [Alerts]
            {context.state.alerts.map(alert => (
              <Alert
                {...alert}
                dismiss={() => context.dismissAlert(alert.index)}
                key={`alert_${alert.index}`}
              />
            ))}
          </section>
        )}
      </AlertContext.Consumer>
    )
  }
}

class Alert extends Component {
  render() {
    let classList = ["alert__item"]

    if (this.props.dismissed) classList.push("dismissed")

    return (
      <div className={classList.join(" ")}>
        [Alert]
        <p>{this.props.message}</p>
        <div>
          <button onClick={() => this.props.dismiss(this.props.index)}>Dismiss</button>
        </div>
      </div>
    )
  }
}

export default App
